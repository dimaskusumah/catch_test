<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Contracts\Filesystem\FileNotFoundException;

class ReadJsonl extends Controller
{
    // Read file
    public function readFile()
    {
        $filename = 'challenge-1-in.jsonl';
        $output = '';

        if (Storage::exists('public/'. $filename)) {
            $output = Storage::get('public/'. $filename);
        } else {
            throw new FileNotFoundException(sprintf('File not found: %s', $filename), 404);
        }

        $ax = explode("\n", $output);

        foreach ($ax as $lines) {
            $obj[] = json_decode($lines, true);
        }

        foreach ($obj as $value) {
            // foreach ($value['items'] as $items) {
            //     $price[] = $items['quantity'] * $items['unit_price'];
            // }

            $req[] = [
                'order_id' => $value['order_id'],
                'order_datetime' => $value['order_date'],
                'customer_state' => $value['customer']['shipping_address']['state'],
                'total_order_value' => $value['items']
            ];
        }

        return $req;
    }
}
